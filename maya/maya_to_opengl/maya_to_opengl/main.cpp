#include <GL/glut.h>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
//#define DEBUG
using namespace std;

// Globals
const char	*CONFIG_FILE = "config.txt",
			*WIN_NAME = "Maya";
const int	FILE_NAME_SIZE = 256,
			LINE_SIZE = 1000,
			VECV_SIZE = 25000,
			VECN_SIZE = 25000,
			VECF_SIZE = 50000;
const float M_PI = 3.14159265f,
			M_EPS = 1e-7f;

// Parameters of window
int winWidth = 600,
	winHeight = 480;

// This is the list of points (3D vectors)
vector<vector<float> > vecv;

// This is the list of normals (also 3D vectors)
vector<vector<float> > vecn;

// This is the list of faces (indices into vecv and vecn)
vector<vector<unsigned> > vecf;

// You will need more global variables to implement color and position changes
GLfloat color[4] = {0.5f, 0.5f, 0.9f, 1.0f};

// light parameters
GLfloat lt0pos1 = 1.0, lt0pos2 = 1.0;

// parameters of camera position
const float dCamSpin = 0.1f; // step of rotation
float camPhi = 0.0f, camPsi = 0.0f, camR = 12.0f;

// parametres of mouse cursor
int mouseX0, mouseY0; // coordinates of mouse's cursor when left button was pressed

// This function is called whenever a "Normal" key press is received.
void keyboardFunc(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27: // Escape key
		exit(0);
		break;
	case 'c': // Change color
		for (int i = 0; i < 3; ++i)
		{
			color[i] += 0.1f;
			if (color[i] >= 1.0f)
				color[i] = 0.0f;
			else
				break;
		}
		break;
	case '-':
		camR += 0.5f;
		break;
	case '=':
		camR -= 0.5f;
		break;
		break;
	default:
		cout << "Unhandled key press " << key << "." << endl;
	}

	// this will refresh the screen so that the user sees the color change
	glutPostRedisplay();
}

// This function is called whenever a "Special" key press is received.
// Right now, it's handling the arrow keys.
void specialFunc(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		lt0pos2 += 0.5f;
		break;
	case GLUT_KEY_DOWN:
		lt0pos2 -= 0.5f;
		break;
	case GLUT_KEY_LEFT:
		lt0pos1 -= 0.5f;
		break;
	case GLUT_KEY_RIGHT:
		lt0pos1 += 0.5f;
		break;
	}

	// this will refresh the screen so that the user sees the light position
	glutPostRedisplay();
}

void mouseFunc(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		cout << "left mouse's button is pressed" << endl;
		mouseX0 = x;
		mouseY0 = y;
	}
	else if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		cout << "left mouse's button is released" << endl;
	}
}

void drawScene(void);
void motionFunc(int x, int y)
{
	float 	dCamPhi = 2 * M_PI * (x - mouseX0) / winWidth,
		dCamPsi = M_PI * (y - mouseY0) / winHeight;
	camPhi += dCamPhi;
	if (camPhi >= 2 * M_PI)
		camPhi -= 2 * M_PI;
	if (camPhi <= 0.0f)
		camPhi += 2 * M_PI;
	camPsi += dCamPsi;
	camPsi = min(camPsi, 0.5f * M_PI - M_EPS);
	camPsi = max(camPsi, -0.5f * M_PI + M_EPS);
	mouseX0 = x;
	mouseY0 = y;
	drawScene();
}

// This function is responsible for displaying the object.
void drawScene(void)
{
	GLfloat 	// Camera's X, Y, Z
		camX = camR * cosf(camPhi) * cosf(camPsi),
		camY = camR * sinf(camPhi) * cosf(camPsi),
		camZ = camR * sinf(camPsi),
		// Material properties of object
		// Specular color and shininess
		specColor[] = {1.0f, 1.0f, 1.0f, 1.0f},
		shininess[] = {100.0f},
		// Light properties
		//Light color (RGBA)
		Lt0diff[] = {1.0f, 1.0f, 1.0f, 1.0f},
		// Light position
		Lt0pos[] = {lt0pos1, lt0pos2, 5.0f, 1.0f};

	cout << "draw Scene" << endl;

	// Clear the rendering window
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Rotate the image
	glMatrixMode(GL_MODELVIEW);  // Current matrix affects objects positions
	glLoadIdentity();              // Initialize to the identity

	// Position the camera at [camX,camZ,camY], looking at [0,0,0],
	// with [0,1,0] as the up direction.
	gluLookAt(	camX, camZ, camY,
				0.0, 0.0, 0.0,
				0.0, 1.0, 0.0);

	cout << "coords " << camX << " " << camY << " " << camZ << endl;
	// Here we use the first color entry as the diffuse color
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color);

	// Note that the specular color and shininess can stay constant
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specColor);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);

	glLightfv(GL_LIGHT0, GL_DIFFUSE, Lt0diff);
	glLightfv(GL_LIGHT0, GL_POSITION, Lt0pos);

#ifdef DEBUG
	// This GLUT method draws a teapot.  You should replace
	// it with code which draws the object you loaded.
	glutSolidTeapot(1.0);
#else
	// draw obj file
	glBegin(GL_TRIANGLES);
	for (unsigned j = 0; j < vecf.size(); ++j)
	{
		// a0, b1, c2, d3, e4, f5, g6, h7, i8
		glNormal3d(vecn[vecf[j][2] - 1][0], vecn[vecf[j][2] - 1][1], vecn[vecf[j][2] - 1][2]);
		glVertex3d(vecv[vecf[j][0] - 1][0], vecv[vecf[j][0] - 1][1], vecv[vecf[j][0] - 1][2]);
		glNormal3d(vecn[vecf[j][5] - 1][0], vecn[vecf[j][5] - 1][1], vecn[vecf[j][5] - 1][2]);
		glVertex3d(vecv[vecf[j][3] - 1][0], vecv[vecf[j][3] - 1][1], vecv[vecf[j][3] - 1][2]);
		glNormal3d(vecn[vecf[j][8] - 1][0], vecn[vecf[j][8] - 1][1], vecn[vecf[j][8] - 1][2]);
		glVertex3d(vecv[vecf[j][6] - 1][0], vecv[vecf[j][6] - 1][1], vecv[vecf[j][6] - 1][2]);
	}
	glEnd();
#endif // DEBUG

	// Dump the image to the screen.
	glutSwapBuffers();
}

// Initialize OpenGL's rendering modes
void initRendering()
{
	glEnable(GL_DEPTH_TEST);   // Depth testing must be turned on
	glEnable(GL_LIGHTING);     // Enable lighting calculations
	glEnable(GL_LIGHT0);       // Turn on light #0.
}

// Called when the window is resized
// w, h - width and height of the window in pixels.
void reshapeFunc(int w, int h)
{
	winWidth = w;
	winHeight = h;
	cout << "new width and size of window: " << winWidth << " " << winHeight << endl;
	// Always use the largest square viewport possible
	if (w > h) {
		glViewport((w - h) / 2, 0, h, h);
	}
	else {
		glViewport(0, (h - w) / 2, w, w);
	}

	// Set up a perspective view, with square aspect ratio
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// 50 degree fov, uniform aspect ratio, near = 1, far = 100
	gluPerspective(50.0, 1.0, 1.0, 100.0);
}

void readConfig(char *objFile) {
	ifstream in(CONFIG_FILE);
	in >> objFile;
}

void loadInput(const char *objFile)
{
	ifstream in(objFile);
	string	line(LINE_SIZE, 0),
		itIs(8, 0);
	istringstream ss(line);
	vector<float> tmpf(3);
	vector<unsigned> tmpi(9);

	vecv.reserve(VECV_SIZE);
	vecn.reserve(VECN_SIZE);
	vecf.reserve(VECF_SIZE);

	while (getline(in, line))
	{
		ss.str(line);
		ss.clear();
		ss >> itIs;
		if (itIs == "v")
		{
			for (int i = 0; i < 3; ++i)
				ss >> tmpf[i];
			vecv.push_back(tmpf);
		}
		else if (itIs == "vn")
		{
			for (int i = 0; i < 3; ++i)
				ss >> tmpf[i];
			vecn.push_back(tmpf);
		}
		else if (itIs == "f")
		{
			for (int i = 0; i < 9; ++i)
			{
				ss >> tmpi[i];
				ss.ignore(1);
			}
			vecf.push_back(tmpi);
		}
	}

	in.close();
}

// Main routine.
// Set up OpenGL, define the callbacks and start the main loop
int main(int argc, char** argv)
{
#ifndef DEBUG
	char objFile[FILE_NAME_SIZE];
	readConfig(objFile);
	loadInput(objFile);
#endif // DEBUG

	glutInit(&argc, argv);

	// We're going to animate it, so double buffer 
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

	// Initial parameters for window position and size
	glutInitWindowPosition(60, 60);
	glutInitWindowSize(winWidth, winHeight);
	glutCreateWindow(WIN_NAME);

	// Initialize OpenGL parameters.
	initRendering();

	// Set up callback functions for key presses
	glutKeyboardFunc(keyboardFunc); // Handles "normal" ascii symbols 
	glutSpecialFunc(specialFunc);   // Handles "special" keyboard keys
	glutMotionFunc(motionFunc); // Handles mouse motions when any mouses's button is pressed
	glutMouseFunc(mouseFunc); // Handles pressing of mouse buttons

	// Set up the callback function for resizing windows
	glutReshapeFunc(reshapeFunc);

	// Call this whenever window needs redrawing
	glutDisplayFunc(drawScene);

	// Start the main loop.  glutMainLoop never returns.
	glutMainLoop();

	return 0;	// This line is never reached.
}