#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cuda.h>
#include <cuda_runtime.h>
#include "check_error.h"
#include "product.h"

int *Init_array(int m) {
	int *X = new int [m];
	
	for(int i = 0; i < m; i++)
		X[i] = rand() % 10;

	return X;
}

int main(void) {
	srand(time(NULL));

	const int n = 2.6E+4;
	
	int *A = Init_array(n * n);
	int *B = Init_array(n);

	int *C = CudaProduct(n, A, B);

	delete [] A;
	delete [] B;
	delete [] C;

	return 0;
}
