#include <iostream>
#include <ctime>
#include "config.h"
#include "check_error.h"
#define TRANSPOSE

__global__ void Transpose(int n, int *X) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	
	int k, tmp;
	while (idx < n) {
		for(k = 1; idx + k < n; k++) {
			tmp = X[(idx + k) * n + idx];
			X[(idx + k) * n + idx] = X[idx * n + idx + k];
			X[idx * n + idx + k] = tmp;
		}

		idx += blockDim.x * gridDim.x;
	}
}

__global__ void Product(int n, int *A, int *B, int *C) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	
	int k;
	while (idx < n) {
		C[idx] = 0;
	
		#ifdef TRANSPOSE
            		for(k = 0; k < n; k++)
                		C[idx] += A[k * n + idx] * B[k];
        	#else
            		for(k = 0; k < n; k++)
                		C[idx] += A[idx * n + k] * B[k];
        	#endif
		
		idx += blockDim.x * gridDim.x;			
	}
}

int *CudaProduct(int n, int *A, int *B) {

	const unsigned matrix_size = n * n * sizeof(int);
	const unsigned vector_size = n * sizeof(int);

	int *cuA, *cuB, *cuC;

	cudaMalloc((void **)&cuA, matrix_size); 
	CheckError("cudaMalloc cuA");
	
	cudaMalloc((void **)&cuB, vector_size); 
	CheckError("cudaMalloc cuB");

	cudaMalloc((void **)&cuC, vector_size); 
	CheckError("cudaMalloc cuC");

	cudaMemcpy(cuA, A, matrix_size, cudaMemcpyHostToDevice);
	CheckError("cudaMemcpy A -> cuA");

	cudaMemcpy(cuB, B, vector_size, cudaMemcpyHostToDevice);
	CheckError("cudaMemcpy B -> cuB");	

	int gridDim, blockDim;
	CudaConfig(n, &gridDim, &blockDim);

	#ifdef TRANSPOSE
		Transpose<<<gridDim, blockDim>>>(n, cuA);
		CheckError("Transpose");
		cudaDeviceSynchronize();
		CheckError("cudaDeviceSynchronize()");
	#endif //TRANSPOSE
	
	struct timespec st, fn;
	clock_gettime(CLOCK_MONOTONIC, &st);

	Product<<<gridDim, blockDim>>>(n, cuA, cuB, cuC);
	CheckError("Product");

	int *C = new int [n];
	
	cudaMemcpy(C, cuC, vector_size, cudaMemcpyDeviceToHost);
	CheckError("cudaMemcpy cuC -> C");

	clock_gettime(CLOCK_MONOTONIC, &fn);
	
	std::cout << "time == ";
	std::cout << fn.tv_nsec - st.tv_nsec;
	std::cout << " nanoseconds\n";

	cudaFree(cuA);
	CheckError("cudaFree cuA");

	cudaFree(cuB);
	CheckError("cudaFree cuB");

	cudaFree(cuC);
	CheckError("cudaFree cuC");

	return C;
}	
