#include <algorithm>
#include <cuda.h>
#include <cuda_runtime.h>
#include "check_error.h"
#include <iostream>
//#define DEBUG

int GCD(int a, int b) {
    if (!b)
        return a;

    return GCD(b, a % b);
}

void CudaConfig(int n, int *gridDim, int *blockDim) {
	cudaDeviceProp prop;
	
	cudaGetDeviceProperties(&prop, 0); 
	CheckError("cudaGetDeviceProperties");

	int gcd = GCD(prop.maxThreadsPerBlock, prop.maxThreadsPerMultiProcessor);

	*blockDim = gcd;

	*gridDim = prop.multiProcessorCount * (prop.maxThreadsPerMultiProcessor / gcd);
	*gridDim = std::min(*gridDim, (n + gcd - 1) / gcd); 	

	#ifdef DEBUG
		std::cout << "blocDim == " <<  *blockDim << std::endl;
		std::cout << "gridDim == " <<  *gridDim << std::endl;
	#endif //DEBUG
}
