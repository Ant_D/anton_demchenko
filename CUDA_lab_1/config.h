#ifndef H_CUDA_CONFIG
#define H_CUDA_CONFIG

void cudaConfig(int n, int *gridDim, int *blockDim);

#endif //H_CUDA_CONFIG
