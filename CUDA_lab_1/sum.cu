#include "config.h"
#include "check_error.h"

__global__ void sum(int n, int *A, int *B, int *C) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	while (idx < n) {
		C[idx] = A[idx] + B[idx];
		idx += blockDim.x * gridDim.x;	
	}
}

void cuSum(int n, int *A, int *B, int *C) {
	int gridDim, blockDim;
	cudaConfig(n, &gridDim, &blockDim);

	sum<<<gridDim, blockDim>>>(n, A, B, C);
	checkError("sum");
}
