#include <iostream>
#include <cuda.h>
#include <cuda_runtime.h>
#include "init.h"
#include "sum.h"
#include "check_error.h"

bool check(int n, int *X) {
	for(int i = 0; i < n; i++)
		if (X[i] != 2 * i)
			return false;

	return true;
}

int main(void) {
	const int n = 2.2E+8;
	const unsigned size = n * sizeof(int);
	
	int *cuA, *cuB, *cuC;

	cudaMalloc((void **)&cuA, size); 
	checkError("cudaMalloc cuA");
	
	cudaMalloc((void **)&cuB, size); 
	checkError("cudaMalloc cuB");
	
	cudaMalloc((void **)&cuC, size); 
	checkError("cudaMalloc cuC");

	cuInit(n, cuA);
	cuInit(n, cuB);
	
	cudaDeviceSynchronize();
	checkError("cudaDeviceSynchronize");
	
	cuSum(n, cuA, cuB, cuC);
	
	int *C = new int [size];
	cudaMemcpy(C, cuC, size, cudaMemcpyDeviceToHost); 
	checkError("cudaMemcpy cuC -> C");

	if (check(n, C))
		std::cout << "Correct answer" << std::endl;
	else
		std::cout << "Wrong answer" << std::endl;

	cudaFree(cuC); 
	checkError("cudaFree cuC");
	
	delete [] C;

	return 0;
}
