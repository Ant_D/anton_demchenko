#include "config.h"
#include "check_error.h"

__global__ void init(int n, int *A) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	while (idx < n) {
		A[idx] = idx;
		idx += blockDim.x * gridDim.x;	
	}
}

void cuInit(int n, int *A) {
	int gridDim, blockDim;
	cudaConfig(n, &gridDim, &blockDim);

	init<<<gridDim, blockDim>>>(n, A);
	checkError("init");
}
