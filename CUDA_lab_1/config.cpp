#include <algorithm>
#include <cuda.h>
#include <cuda_runtime.h>
#include "check_error.h"

void cudaConfig(int n, int *gridDim, int *blockDim) {
	cudaDeviceProp prop;
	
	cudaGetDeviceProperties(&prop, 0); 
	checkError("cudaGetDeviceProperties");

	*gridDim = std::min(prop.maxGridSize[0], (n + prop.maxThreadsPerBlock - 1) / prop.maxThreadsPerBlock); 	
	
	int nWarps = (n + prop.warpSize - 1) / prop.warpSize;
	*blockDim = std::min(prop.maxThreadsPerBlock, nWarps * prop.warpSize);
}
