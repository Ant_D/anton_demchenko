#ifndef H_CUDA_CONFIG
#define H_CUDA_CONFIG

void CudaConfig(int n, int *gridDim, int *blockDim);

void CudaConfigSpecial(int n, dim3 *gridDim, dim3 *blockDim);

#endif //H_CUDA_CONFIG
