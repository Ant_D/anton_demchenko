#include <iostream>
#include "transpose.h"
//#define CHECK

int *Init_array(int m) {
	int *X = new int [m];
	
	for(int i = 0; i < m; i++)
		X[i] = i;

	return X;
}

bool Check(int n, int *A, int *B) {
	int i, j;
	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)
			if (A[i * n + j] != B[j * n + i])
				return false;
	return true;
}

int main(void) {
	const int n = 16000;
	
	int *A = Init_array(n * n);

	int *B = CudaTranspose(n, A);

	#ifdef CHECK
		if (Check(n, A, B))
			std::cout << "Correct answer\n";
		else
			std::cout << "Wrong answer\n";
	#endif

	delete [] A;
	delete [] B;

	return 0;
}
