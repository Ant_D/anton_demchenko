#ifndef CUDA_TRANSPOSE
#define CUDA_TRANSPOSE

int *CudaTranspose(int n, int *A);

#endif //CUDA_TRANSPOSE
