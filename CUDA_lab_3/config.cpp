#include <algorithm>
#include <cuda.h>
#include <cuda_runtime.h>
#include "check_error.h"
#include <iostream>
//#define DEBUG

int GCD(int a, int b) {
    if (!b)
        return a;

    return GCD(b, a % b);
}

void CudaConfig(int n, int *gridDim, int *blockDim) {
	cudaDeviceProp prop;
	
	cudaGetDeviceProperties(&prop, 0); 
	CheckError("cudaGetDeviceProperties");

	int gcd = GCD(prop.maxThreadsPerBlock, prop.maxThreadsPerMultiProcessor);

	*blockDim = gcd;

	*gridDim = prop.multiProcessorCount * (prop.maxThreadsPerMultiProcessor / gcd);
	*gridDim = std::min(*gridDim, (n + gcd - 1) / gcd); 	

	#ifdef DEBUG
		std::cout << "blocDim == " <<  *blockDim << std::endl;
		std::cout << "gridDim == " <<  *gridDim << std::endl;
	#endif //DEBUG
}

void CudaConfigSpecial(int n, dim3 *gridDim, dim3 *blockDim) {
	cudaDeviceProp prop;
	
	cudaGetDeviceProperties(&prop, 0); 
	CheckError("cudaGetDeviceProperties");

	blockDim->x = blockDim->y = 16;

	gridDim->x = prop.multiProcessorCount * (prop.maxThreadsPerMultiProcessor / 16);
	gridDim->x = std::min((int)gridDim->x, (n + 256 - 1) / 256); 	

	#ifdef DEBUG
		std::cout << "blocDim.x == " <<  blockDim->x << std::endl;
		std::cout << "blocDim.y == " <<  blockDim->y << std::endl;
		std::cout << "gridDim.x == " <<  gridDim->x << std::endl;
	#endif //DEBUG

}
