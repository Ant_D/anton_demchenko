#include <iostream>
#include <ctime>
#include "config.h"
#include "check_error.h"
#define BLOCK_SIZE 16
#define SHARED_MEMORY

__global__ void Transpose(int n, int *A, int *B) {
#ifdef SHARED_MEMORY
	__shared__ int tmp[BLOCK_SIZE][BLOCK_SIZE];

	int b = n / BLOCK_SIZE;	
	int bid = blockIdx.x;

	int bx, by, x, y;
	while (bid < b * b ) {
		bx = bid % b;
		by = bid / b;
		x = bx * BLOCK_SIZE;
		y = by * BLOCK_SIZE;

		tmp[threadIdx.y][threadIdx.x] = A[(y + threadIdx.y) * n + x + threadIdx.x];

		syncthreads();

		B[(x + threadIdx.y) * n + y + threadIdx.x] = tmp[threadIdx.x][threadIdx.y];

		bid += gridDim.x;
	}
#else
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	
	int i, j;
	while (idx < n * n) {
		i = idx / n;
		j = idx % n;	
		B[j * n + i] = A[i * n + j];

		idx += blockDim.x * gridDim.x;
	}
#endif //SHARED_MEMORY
}

int *CudaTranspose(int n, int *A) {

	const unsigned size = n * n * sizeof(int);

	int *B = new int [n * n];
	int *cuA, *cuB;

	cudaMalloc((void **)&cuA, size); 
	CheckError("cudaMalloc cuA");

	cudaMalloc((void **)&cuB, size);
	CheckError("cudaMalloc cuB");

	cudaMemcpy(cuA, A, size, cudaMemcpyHostToDevice);
	CheckError("cudaMemcpy A -> cuA");

	#ifdef SHARED_MEMORY
		dim3 gridDim, blockDim;
		CudaConfigSpecial(n * n, &gridDim, &blockDim);
	#else
		int gridDim, blockDim;
		CudaConfig(n * n, &gridDim, &blockDim);
	#endif //SHARED_MEMORY
	
	struct timespec st, fn;
	clock_gettime(CLOCK_MONOTONIC, &st);

	Transpose<<<gridDim, blockDim >>>(n, cuA, cuB);
	CheckError("Transpose");	
		
	cudaMemcpy(B, cuB, size, cudaMemcpyDeviceToHost);
	CheckError("cudaMemcpy cuB -> B");

	clock_gettime(CLOCK_MONOTONIC, &fn);
	
	std::cout << "time == " << fn.tv_nsec - st.tv_nsec << " nsec\n";
	std::cout << "time == " << fn.tv_sec - st.tv_sec << " sec\n";

	cudaFree(cuA);
	CheckError("cudaFree cuA");

	cudaFree(cuB);
	CheckError("cudaFree cuB");

	return B;
}	
