#include <iostream>
#include <string>
#include <cuda.h>
#include <cuda_runtime.h>

void CheckError(std::string err){
	if(cudaGetLastError() != cudaSuccess){
		std::cout << "ERROR: " + err << std::endl;
		exit(0);
	}
}
