#include "PrintStr.h"
#include <mutex>
#include <iostream>

// Мьютекс для стандартного потока вывода
std::mutex mCout;

// Потокобезопасный cout строки str
void PrintStr(const char* str) {
    std::lock_guard<std::mutex> guard(mCout);
    std::cout << str << std::endl;
}
