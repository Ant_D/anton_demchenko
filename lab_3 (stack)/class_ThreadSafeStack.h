#ifndef H_THREAD_SAFE_STACK
#define H_THREAD_SAFE_STACK

#include <mutex>
#include <memory>
#include <stack>

template<class T>
class ThreadSafeStack {
public:
    // Конструктор
    ThreadSafeStack();

    // Деструктор
    ~ThreadSafeStack();

    // Конструктор копирования
    ThreadSafeStack(const ThreadSafeStack<T>& obj) = delete;

    // Оператор присваивания
    ThreadSafeStack& operator = (const ThreadSafeStack<T>& obj) = delete;

    // Добавить элемент в начало стека
    void push(const T& obj);

    // Перегруженные функции удаления и получения первого элемента стека
    std::shared_ptr<T> pop_top();
    void pop_top(T& result);

private:
    // Стандартный стек
    std::stack<T> _st;

    //Мьютекс для стека
    std::mutex _m;
};

// Конструктор
template<class T>
ThreadSafeStack<T>::ThreadSafeStack() {}

// Деструктор
template<class T>
ThreadSafeStack<T>::~ThreadSafeStack() {}

// Добавить элемент в начало стека
template<class T>
void ThreadSafeStack<T>::push(const T& obj) {
    std::lock_guard<std::mutex> guard(_m);
    _st.push(obj);
}

// Удаляет первый элемент и возвращает указатель на него
// В случае пустого стека возвращается nullptr
template<class T>
std::shared_ptr<T> ThreadSafeStack<T>::pop_top() {
    std::lock_guard<std::mutex> guard(_m);
    std::shared_ptr<T> ptr;
    // Избегание исключения в случае, когда стек пуст
    if (!_st.empty()) {
        ptr = std::make_shared<T>(_st.top());
        _st.pop();
    }
    return ptr;
}

// Удаляет первый элемент и записывает его в result
// В случае пустого стека result не меняется
template<class T>
void ThreadSafeStack<T>::pop_top(T& result) {
    std::lock_guard<std::mutex> guard(_m);
    // Избегание исключения в случае, когда стек пуст
    if (!_st.empty()) {
        result = _st.top();
        _st.pop();
    }
}

#endif //H_THREAD_SAFE_STACK
