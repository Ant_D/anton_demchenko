#include "class_Place.h"
#include <cstring>

// Пустой конструктор
Place::Place() {
	strcpy(_name, "");
}

// Конструктор
Place::Place(char* name){
	strcpy(_name, name);
}

// Деструктор
Place::~Place() {}


// Конструктор копирования
Place::Place(const Place& obj){
	strcpy(_name, obj.getName());
}

// Оператор присваивания
Place& Place::operator = (const Place& obj){
	strcpy(_name, obj.getName());
	return *this;
}

// Получить название места
const char* Place::getName() const{
	return &_name[0];
}
