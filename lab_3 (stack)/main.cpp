#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
#include "class_ThreadSafeStack.h"
#include "class_ThreadGuard.h"
#include "threadFunction.h"
#include "class_Place.h"
#include "PrintStr.h"

int main(void) {
	ThreadSafeStack<Place> st;

    // Потоки будут иметь данные имена
	const char* name[] = {"Vasiliy", "Ivan", "Pyotr"};
	ThreadGuard t_guard[3];

	std::ifstream in("input.txt");
	char str[256];

    // Стек сначала заполняется из файла
    while (!in.eof()){
		in.getline(str, 255);
		st.push(Place(str));
	}
	in.close();

    // Запускаются потоки, работающие со стеком
	for(int i = 0; i < 3; i++)
		t_guard[i](std::thread(threadFunction, name[i], std::ref(st)));

    // Ожидание завершения работы потоков
	for(int i = 0; i < 3; i++)
		t_guard[i].join();

    // В конце работы потоков стек будет пустым
    // Проверка состояния стека
    if (st.pop_top() == nullptr) PrintStr("Stack is empty");
    else PrintStr("Stack is not empty");

	return 0;
}
