#ifndef H_PLACE
#define H_PLACE

class Place {
public:
    // Пустой конструктор
	Place();

    // Конструктор
	Place(char* name);

	// Деструктор
	~Place();

	// Конструктор копирования
	Place(const Place& obj);

    // Оператор присваивания
	Place& operator = (const Place& obj);

    // Получить название места
	const char* getName() const;

private:
    // Название места
	char _name[256];
};

#endif //H_PLACE
