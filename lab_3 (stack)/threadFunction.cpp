#include "threadFunction.h"
#include "PrintStr.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>

using std::endl;

// Функция, выполняемая потоками
void threadFunction(const char* name, ThreadSafeStack<Place>& st) {
    // Создание лог-файла
    char fileName[256];
    strcpy(fileName, name);
    strcat(fileName, "_logfile");
	std::ofstream out(fileName);

	std::shared_ptr<Place> P;

    // Цикл завершится, когда стек будет пустым
	while ((P = st.pop_top()) != nullptr) {
		out << "pop_top() " << P->getName() << endl;
        // Выбирается, оставить или вернуть полученный объект
		if (rand() % 2) {
			st.push(*P);
			out << "push() " << P->getName() << endl;
		}
		else {
            // Вывод сообщения о завершении обработки объекта *P
            char str[256];
            strcpy(str, name);
            strcat(str, " -> ");
            strcat(str, P->getName());
			PrintStr(str);
		}
	}

	out << "pop_top() nullptr" << endl;
	out.close();
}
