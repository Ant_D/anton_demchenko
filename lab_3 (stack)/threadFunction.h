#ifndef H_THREAD_FUNCTION
#define H_THREAD_FUNCTION

#include "class_ThreadSafeStack.h"
#include "class_Place.h"

// Функция, выполняемая потоками
void threadFunction(const char* name, ThreadSafeStack<Place>& st);

#endif // H_THREAD_FUNCTION
